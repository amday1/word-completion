import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class Trie {

	private static TrieNode root;
	private Trie t;

	/**
	 * creates a new TrieNode at the root
	 */
	public Trie() {
		root = new TrieNode();
	}

	public TrieNode getRoot() {
		return root;
	}

	public Trie getTrie() {
		return t;
	}

	/**
	 * 
	 * @param word
	 * @return true if word was added successfully
	 */
	public boolean add(String word) {
		// if word exists return false
		if (contains(word)) {
			return false;
		} else {
			TrieNode node = getRoot();
			char currentCharacter;
			// loop through every letter in word
			// if current character already exists, update current character
			// if it doesn't exist, create node and then update current
			// character
			for (int i = 0; i < word.length(); i++) {
				currentCharacter = word.charAt(i);
				if (node.getOffspring(currentCharacter) == null) {
					node.setOffspring(currentCharacter);
					if (i == word.length() - 1) {
						node.getOffspring(currentCharacter).setEnd(true);
					}
				}
				node = node.getOffspring(currentCharacter);
			}
			return true;
		}
	}

	/**
	 * 
	 * @param word
	 * @return true if the word is contained
	 */
	public boolean contains(String word) {
		TrieNode p = getRoot();
		boolean contains = false;
		char currentChar;
		char lastChar = word.charAt(word.length() - 1);
		for (int i = 0; i < word.length(); i++) {
			currentChar = word.charAt(i);
			// System.out.println("CURRENT CHAR: " + currentChar);
			if (p.getOffspring(currentChar) != null) {
				if (i == word.length() - 1 && p.getOffspring(lastChar).isEnd()) {
					contains = true;
				}
				// updates to new node
				p = p.getOffspring(currentChar);
			}
		}
		// System.out.println("LAST CHAR: " + lastChar + "\n");
		return contains;
	}

	/**
	 * Start at root and check for offspring. If offspring exists, add it to the
	 * queue. Whilst the queue is not empty, create a new node. Compare this
	 * node to current node character.
	 * 
	 * expecting: bcaahttaetersse
	 * 
	 * @return string contains breadth first search result
	 */
	public String outputBreadthFirstSearch() {
		String str = "";

		TrieNode node = getRoot();
		if (root == null) {
			return "";
		}

		// create queue and add node
		Queue<TrieNode> q = new LinkedList<>();
		q.add(node);

		// while q is not empty, create temp node and create array of its
		// children
		while (!q.isEmpty()) {
			TrieNode temp = q.remove();
			TrieNode[] tempArray = temp.getOffspringArray();
			for (int i = 0; i < tempArray.length; i++) {
				if (tempArray[i] != null) {
					str += tempArray[i].getChar();
					q.add(tempArray[i]);
				}
			}
		}
		return str;
	}

	/**
	 * Create stack. Add node to stack. Look at next node. Push next node to
	 * stack. If there is no offspring, pop off the stack (add to string)
	 * 
	 * expecting: tabtatasreseehc
	 * 
	 * @return string containing depth first search result
	 */
	public String outputDepthFirstSearch() {
		String str = "";

		ArrayList<TrieNode> list = new ArrayList<>();
		if (root == null) {
			return "";
		}

		// create stack
		Stack<TrieNode> st = new Stack<TrieNode>();
		st.push(getRoot());
		while (!st.isEmpty()) {
			// peek at node
			TrieNode temp = st.peek();
			// add to list to set back to not seen later for running a depth
			// first search twice
			list.add(temp);
			// get offspring array
			TrieNode[] tempArray = temp.getOffspringArray();
			boolean allSeen = true;
			// loop through offspring array
			for (int i = 0; i < tempArray.length; i++) {
				// if the array contains an item and it hasn't been seen
				if (tempArray[i] != null && !tempArray[i].hasSeen()) {
					// add to stack
					st.push(tempArray[i]);
					// state all offspring have not been seen
					allSeen = false;
					break;
				}
			}
			// if all offspring have been seen, add to string and pop from stack
			if (allSeen == true) {
				str += temp.getChar();
				temp.setSeen(true);
				st.pop();
			}
		}
		// set all nodes seen back to false
		for (TrieNode node : list) {
			node.setSeen(false);
		}
		return str;
	}

	/*
	 * check prefix exists before starting
	 */
	public Trie getSubTrie(String prefix) {
		TrieNode node = getRoot();
		Trie newTrie = new Trie();
		char[] cArray = prefix.toCharArray();

		for (int i = 0; i < cArray.length; i++) {
			char c = cArray[i];
			node = node.getOffspring(c);
			if (i == prefix.length() - 1) {
				newTrie.getRoot().setAllOffspring(node.getOffspringArray());
			}
		}
		return newTrie;
	}

	public static void printTrie(TrieNode node, int offset) {
		TrieNode[] array = node.getOffspringArray();
		for (int i = 0; i < offset; i++) {
			System.out.print(" ");
		}
		System.out.println(node.getChar());
		for (TrieNode temp : array) {
			if (temp != null) {
				printTrie(temp, offset + 2);
			}
		}
	}

	/*
	 * start at root get offspring until end of word add word to string add
	 * string to list clear string
	 */
	public List<String> getAllWords() {

		List<TrieNode> list = new ArrayList<>();
		List<String> wordList = new ArrayList<>();

		if (root == null) {
			return null;
		}

		// create stack
		Stack<TrieNode> st = new Stack<TrieNode>();
		st.push(getRoot());
		while (!st.isEmpty()) {
			// peek at node
			TrieNode temp = st.peek();
			// add to list to set back to not seen later for running a depth
			// first search twice
			list.add(temp);
			// get offspring array
			TrieNode[] tempArray = temp.getOffspringArray();
			boolean allSeen = true;
			// loop through offspring array
			for (int i = 0; i < tempArray.length; i++) {
				// if the array contains an item and it hasn't been seen
				if (tempArray[i] != null && !tempArray[i].hasSeen()) {
					// add to stack
					st.push(tempArray[i]);
					// state all offspring have not been seen
					allSeen = false;
					break;
				}
			}
			// if all offspring have been seen, add to string and pop from stack
			if (allSeen == true) {
				if (temp.isEnd()) {
					String word = "";
					for (TrieNode s : st) {
						word = word.trim() + s.getChar();
					}
					wordList.add(word);
				}
				temp.setSeen(true);
				st.pop();
			}
		}
		// set all nodes seen back to false
		for (TrieNode node : list) {
			node.setSeen(false);
		}
		return wordList;
	}

	public static void main(String[] args) {
		Trie t = new Trie();

		// testing for add and contains methods
		// change later to make more verbose
		System.out.println("##############################################\n");

		System.out.println("cheers added, returns: " + t.add("cheers"));
		System.out.println("cheese added, returns: " + t.add("cheese"));
		System.out.println("chat added, returns: " + t.add("chat"));
		System.out.println("cat added, returns: " + t.add("cat"));
		System.out.println("bat added, returns: " + t.add("bat") + "\n");
		System.out.println("Does trie contain 'cat': " + t.contains("cat"));

		System.out
				.println("\n##############################################\n");

		System.out.println("Breadth First Search: "
				+ t.outputBreadthFirstSearch());
		System.out.println("Depth First Search: " + t.outputDepthFirstSearch());
		System.out
				.println("Depth First Search again to check each node has been reset: "
						+ t.outputDepthFirstSearch());

		System.out
				.println("\n##############################################\n");

		System.out
				.println("Additional print method implemented to see nodes added correctly");
		t.printTrie(root, 0);

		System.out
				.println("\n##############################################\n");
		System.out.println("All words from Trie returned");
		System.out.println(t.getAllWords());

		System.out
				.println("\n##############################################\n");
		System.out.println("Sub Trie with the prefix 'ch': ");
		printTrie(t.getSubTrie("ch").getRoot(), 0);

		System.out
				.println("\n##############################################\n");
	}
}