import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

public class DictionaryMaker {
	private static ArrayList<String> words;
	private static TreeMap<String, Integer> map;

	public ArrayList<String> getWords() {
		return words;
	}

	/**
	 * Reads all the words in a comma separated text document into an Array
	 * 
	 * @param f
	 */
	public ArrayList<String> readWordsFromFile(String file)
			throws FileNotFoundException {
		Scanner sc = new Scanner(new File(file));
		words = new ArrayList<String>();
		sc.useDelimiter(",");
		String str;
		while (sc.hasNext()) {
			str = sc.next();
			str = str.trim();
			str = str.toLowerCase();
			str = str.replaceAll("[^A-Za-z]+", "");
			words.add(str);
		}
		return words;
	}

	/**
	 * Creates a new dictionary object
	 */
	public DictionaryMaker() {
		map = new TreeMap<String, Integer>();
	}

	/**
	 * Creates the dictionary in a TreeMap, setting the frequency to 1 if there
	 * is a new word being added, or incrementing the frequency if the word
	 * already exists
	 * 
	 * @param words
	 * @return map
	 */
	public TreeMap<String, Integer> FormDictionary(ArrayList<String> words) {
		map = new TreeMap<String, Integer>();
		for (String w : words) {
			if (map.containsKey(w) == false) {
				map.put(w, 1);
			} else {
				map.put(w, map.get(w) + 1);
			}
		}
		return map;
	}

	/**
	 * Saves the tree map to a file
	 * 
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public void saveToFile() throws FileNotFoundException,
			UnsupportedEncodingException {
		String textFile = "testDictionary.csv";
		PrintWriter writer = new PrintWriter(textFile, "UTF-8");
		for (Entry<String, Integer> entry : map.entrySet()) {
			writer.write(entry.getKey() + "," + entry.getValue() + "\n");
		}
		System.out.println("Finished writing to file " + textFile);
		writer.close();
	}

	/**
	 * Returns the tree map
	 * @return map
	 */
	public TreeMap<String, Integer> getMap() {
		return map;
	}

	public static void main(String[] args) throws FileNotFoundException,
			UnsupportedEncodingException {

		DictionaryMaker d = new DictionaryMaker();
		d.readWordsFromFile("U:\\eclipse-workspace\\DSA2\\testDocument.csv");
		d.FormDictionary(words);
		d.saveToFile();
	}
}
