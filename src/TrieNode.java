public class TrieNode {
	private char s;
	private boolean end;
	private boolean seen;
	private final int MAX_LENGTH = 26;
	private TrieNode[] offspring;

	/**
	 * Default constructor for a TrieNode, used for making the root
	 */
	public TrieNode() {
		offspring = new TrieNode[MAX_LENGTH];
		this.seen = false;
		this.end = false;
	}

	/**
	 * Constructor for a TrieNode with a character
	 * 
	 * @param s
	 */
	public TrieNode(char s) {
		this.s = s;
		this.end = false;
		this.seen = false;
		offspring = new TrieNode[MAX_LENGTH];
	}

	/**
	 * Gets the offspring of a node using the character of the node
	 * 
	 * @param s
	 * @return offspring at position character
	 */
	public TrieNode getOffspring(char s) {
		if (offspring[convertToInt(s)] != null) {
			return offspring[convertToInt(s)];
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @return offspring array
	 */
	public TrieNode[] getOffspringArray() {
		return offspring;
	}

	/**
	 * converts character to ascii value used to convert character into position
	 * 0-26 in array
	 * 
	 * @param s
	 * @return int for character position
	 */
	public int convertToInt(char s) {
		return ((int) s) - 97;
	}

	/**
	 * return character a node
	 * 
	 * @return character
	 */
	public char getChar() {
		return s;
	}

	/**
	 * set method for a character
	 * 
	 * @param s
	 */
	public void setChar(char s) {
		this.s = s;
	}

	/**
	 * set offspring for a node using a character
	 * 
	 * @param s
	 */
	public void setOffspring(char s) {
		getOffspringArray()[convertToInt(s)] = new TrieNode(s);
	}

	/**
	 * sets all offspring to a node, used for sub trie
	 * 
	 * @param offspring
	 */
	public void setAllOffspring(TrieNode[] offspring) {
		this.offspring = offspring;
	}

	/**
	 * returns true if the node is at the end of a word
	 * 
	 * @return end
	 */
	public boolean isEnd() {
		return end;
	}

	/**
	 * set node as the end of a word
	 * 
	 * @param end
	 */
	public void setEnd(boolean end) {
		this.end = end;
	}

	/**
	 * set node to seen when traversing
	 * 
	 * @param seen
	 */
	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	/**
	 * returns whether the node has been seen or not
	 * 
	 * @return seen
	 */
	public boolean hasSeen() {
		return seen;
	}
}