import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class AutoCompletion {

	public static void main(String[] args) throws IOException {
		// create new dictionary
		DictionaryMaker d = new DictionaryMaker();
		String lotr = "lotr.csv";
		// form dictionary from provided csv file
		d.FormDictionary(d.readWordsFromFile(lotr));

		// create two tries, the original trie, and a temp trie used later on
		// for the subtrie
		Trie t = new Trie();
		Trie tempTrie = new Trie();

		// for each word in the map, add it to the trie
		for (String s : d.getWords()) {
			t.add(s);
		}

		// create printwriter and stringbuilder for adding results to csv
		PrintWriter pw = new PrintWriter(new File("lotrMatches.csv"));
		StringBuilder sb = new StringBuilder();

		String lotrQueries = "lotrQueries.csv";
		String line = "";
		String prefix = "";
		BufferedReader lotrLine = new BufferedReader(
				new FileReader(lotrQueries));

		// while there is a line to read
		while ((line = lotrLine.readLine()) != null) {
			// highly inefficient use, however I could not find another way to
			// reset the trie each time
			t = new Trie();
			for (String s : d.getWords()) {
				t.add(s);
			}
			// create a second tree map in order to only store words with the
			// correct prefix
			Map<Integer, String> treeMap = new TreeMap<Integer, String>(
					Collections.reverseOrder());
			String[] words = line.split(",");
			// for each word, make subtrie at prefix
			for (String w : words) {
				prefix = w;
				System.out.println("Prefix: " + prefix);
				tempTrie = tempTrie.getSubTrie(w);
				for (String str : tempTrie.getAllWords()) {
					String completeWord = w + str;
					// if the word is contained in the original map, put it into
					// new map so it can be ordered
					if (d.getMap().containsKey(completeWord)) {
						treeMap.put((int) d.getMap().get(completeWord),
								completeWord);
					}
				}
			}
			double numValues = 0;
			// used to calculate number of occurrences of all words with given
			// prefix
			for (Map.Entry<Integer, String> entry : treeMap.entrySet()) {
				Integer key = entry.getKey();
				numValues += key;
			}
			System.out.println("total: " + numValues);
			System.out.println("\n");

			int count = 0;
			String value = "";
			double probability = 0;
			sb.append(prefix);
			sb.append(",");
			// used to get the top 3 values from the tree map (or fewer if there
			// is not a top 3)
			for (Map.Entry<Integer, String> entry : treeMap.entrySet()) {
				if (count == 3) {
					break;
				}
				count++;
				Integer key = entry.getKey();
				value = entry.getValue();
				probability = key / numValues;
				sb.append(value);
				sb.append(",");
				sb.append(probability);
				sb.append(",");
				System.out.println("Value: " + value);
				System.out.println("Probability: " + probability);
			}
			System.out.println("\n");
			sb.append("\n");
			pw.write(sb.toString());
		}
		lotrLine.close();
		pw.close();
	}
}
