# Word Completion

Your assignment involves developing a simplified system for auto-completion of words. The assignment
is in three stages:

1. read in a document of words, find the set of unique words to form a dictionary, count the occurrence of each word, then save the words and counts to file;

2. define a trie data structure for storing a suffix tree and design and implement algorithms to manipulate the trie; and

3. make a word completion system using the dictionary from part 1) and the data structure from part
